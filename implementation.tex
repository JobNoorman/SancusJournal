\section{Implementation}
\label{sec:implementation}

This \lcnamecref{sec:implementation} discusses the implementation of Sancus.
We have implemented hardware support for all security features discussed in \cref{sec:design}, as well as a compiler that can create software modules
suitable for deployment on the hardware.

\subsection{The Processor}
\label{sec:implementation:mcu}

Our hardware implementation is based on an open source implementation of the TI MSP430 architecture: the \omsp{} from the OpenCores project~\cite{openMSP430}.
We have chosen this architecture because both GCC and LLVM support it, and there exists a lot of software running natively on the MSP430, for example the Contiki operating system.

The discussion is organized as follows.
First, we explain the features added to the \omsp{} in order to implement the isolation of software modules.
Then, we discuss how we added support for the cryptographic operations.
Finally, we describe the modifications we made to the \omsp{} core itself.

\subsubsection{Isolation}

This part of the implementation deals with enforcing the access rights shown in \Cref{flt:access_rights}.
For this purpose, the processor needs access to the layout of every software module that is currently protected.
Since the access rights need to be checked on every instruction, accessing these values should be as fast as possible.
For this reason, we have decided to store the layout information in special registers inside the processor.
Note that this means the total number of software modules that can be protected at any particular time has a fixed upper bound.
This upper bound, $N_{\SM}$, can be configured when synthesizing the processor.

\Cref{flt:sm_hw} gives an overview of the Memory Access Logic (MAL) circuit used to enforce the access rights of a single software module.
This MAL circuit is instantiated $N_{\SM}$ times in the processor.
It has five inputs: \code{pc} and \code{prev\_pc} are the current and previous values of the program counter, respectively.
The input \code{mab} is the memory address bus -- the address currently used for load or store operations%
\footnote{Of course, this includes implicit memory accesses like a \code{call} instruction.}
-- while \code{mb\_en} indicates whether the address bus is enabled for the current instruction and \code{mb\_wr} whether the access is a write.
The MAL circuit has one output, \code{violation}, that is asserted whenever one of the access rules is violated.

\begin{figure}
  \centering
  \maxsizebox{.9\linewidth}{!}{\input{images/mal.tex}}
  \caption{Schematic of the Memory Access Logic (MAL), the hardware used to enforce the memory access rules for a single protected module.}
  \label{flt:sm_hw}
\end{figure}

Apart from the input and output signals, the MAL circuit also keeps state in registers.
The layout of the protected software module is captured in the \code{TS} (start of text section), \code{TE} (end of text section), \code{DS} (start of data section) and \code{DE} (end of data section) registers.
The \code{EN} register is set to $1$ if there is currently a module being protected by this MAL circuit instantiation.
The layout is saved in the registers when the \code{protect} instruction is called, at which time \code{EN} is also set.
When the \code{unprotect} instruction is called, we just unset \code{EN} which disables all checks.

Since the circuit is purely combinational, no extra cycles are needed for the enforcement of access rights.
As explained above, this is exactly what we want since these rights need to be checked for every instruction.
The only downside this approach might have is that the large combinational circuit adds to the length of the critical path of the processor.
We will explore the implications our design has on the processor's critical path in  \cref{sec:evaluation:performance}.

Apart from hardware circuit blocks that enforce the access rights, we also added a single hardware circuit to control the MAL circuit instantiations.
It implements four tasks:
\begin{paraenum}
  \item combine the \code{violation} signals from every MAL instantiation into a single signal;
  \item keep track of the value of the current and previous program counter;
  \item keep track of the currently and previously executing $\SM$; and
  \item when the \code{protect} instruction is called, select a free MAL instantiation to store the layout of the new software module and assign it a unique ID.
\end{paraenum}

\subsubsection{Cryptography}
\label{sec:implementation:crypto}

As explained in \cref{sec:design:crypto}, a hardware implementation of three cryptographic primitives is needed to implement our design: authenticated encryption, key derivation and hashing.
Since our implementation is based on a small microprocessor, one of our main goals here is to make the implementation of these features as small as possible.

We have chosen to build these cryptographic primitives on the \spongewrap~\cite{spongewrap} authenticated encryption construction using \spongent~\cite{spongent} as the underlying sponge function.
Since keyed sponge functions are shown to be pseudorandom functions~\cite{keyedsponge}, we can reuse \spongewrap{} to calculate \acp{MAC}, and consequently for key derivation.
Since the security of \spongewrap{} relies on the soundness of the sponge function it uses, it can also be used as a hashing function by calling $\aee(\{\}, \{\}, M)$.

Besides being able to use it for all necessary primitives, there are several reasons we use \spongewrap{} with \spongent.
Since the security of \spongewrap{} is proportional to the capacity of the underlying sponge function%
\footnote{To be exact, for a sponge function with a capacity of $c$ bits, \spongewrap{} has a security of $c/2$ bits~\cite{spongewrap}.}%
, and \spongent{} is defined for a large range of capacities, we can create an implementation with a selectable security parameter.
More specifically, our core can be synthesized with a security parameter between 16 and 256 bits, although values less than 80 bits should be avoided.
Since the security parameter influences the core's area (\cref{sec:evaluation:area}), it is a trade-off between cost and security.

As we will see later, all module keys are stored in hardware making the key size an important design parameter regarding area.
Another advantage of \spongewrap{} is that the key size may be as small as the security parameter whereas some other lightweight authenticated encryption primitives require a key that is twice as long, e.g., APE~\cite{ape}.

A downside of \spongewrap{} is that uniqueness of the associated data is required for confidentiality, and no security guarantees can be given when a nonce is reused.
More specifically, if two ciphertext messages are captured that are encrypted with the same key and associated data, part of the XOR of the corresponding plaintext message may be leaked (see~\cite{spongewrap} for details).
Therefore, the user of this primitive should ensure that, for a specific key, the associated data is unique, i.e., that it includes a nonce.
Note that this is only necessary when encrypting data and there is no nonce requirement for creating \acp{MAC}.
In contrast, \textit{nonce-misuse resistant} authenticated encryption algorithms (e.g., APE mentioned above) limit information leakage about the message when the nonce is reused, but this comes at an additional implementation cost.

It is a software provider's responsibility that the nonce requirement is fulfilled by the modules it deploys.
In our prototypes, this is achieved by having $\SP$ send an initial counter value as nonce in its first message to a newly deployed module.
For subsequent messages, modules can simply increment the counter and use that value as the next nonce.
Alternatively, if $\SP$ never wants to send messages to a module, the initial counter value can be included in the module's text section.

The node key $K_N$ is fixed when the hardware is synthesized and should be created using a secure random number generator.
When a module $\SM$ is loaded, the processor will first derive $K_{N,\SP}$ using the \spongewrap{} implementation which is then used to derive $K_{N,\SP,\SM}$.
The latter key will then be stored in the hardware MAL instantiation for the loaded module.
Note that we have chosen to cache the module keys instead of calculating them on the fly whenever they are needed.
This is a trade-off between size and performance which we feel is justified because, when using 128 bit keys, \spongewrap{} needs about $90$ cycles per input byte (\cref{sec:evaluation:performance}).
Since the module key is needed for every remote attestation and whenever the module's output needs to be encrypted, having to calculate it on the fly would introduce a runtime overhead that we expect to be too high for most applications.

Because of the associated data uniqueness requirement explained above, our implementation of confidential loading is slightly different from its design (\cref{sec:design:confidential-loading}).
Since modules deployed on $N$ by $\SP$ are always encrypted using $K_{N,\SP}$, the \instr{protect} instruction takes an extra argument, \instrarg{nonce}, to be able to fulfill the nonce requirement.
This argument is used as the associated data input for the decryption routine.

\subsubsection{Core Modifications}

The largest modification that had to be made to the core is the decoding of the new instructions.
We have identified a range of opcodes, starting at 0x1380, that is unused in the MSP430 instruction set and mapped the new instructions in that range.

Further modifications include routing the needed signals, like the memory address bus, into the access rights modules as well as connecting the violation signal to the internal reset.
Note that the violation signal is stored into a register before connecting it to the reset line to avoid the asynchronous reset being triggered by combinational glitches from the MAL circuit.

Since our experience has shown that developing applications on a system that resets on violations is rather tedious, we added a synthesis option to generate a non-maskable interrupt instead.
If this option is enabled, the memory backbone will disable all memory accesses when a violation is generated and the frontend will initiate the IRQ sequence.
Although this may superficially seem secure, it brings with it a number of problems (e.g., if a module generates a violation, its register contents will be leaked) we have not dealt with yet.
Therefore, it currently is not enabled by default and should not be used in production environments.

\Cref{flt:hwoverview} gives an overview of the added hardware blocks when synthesized with support for two protected modules.
In order to keep the figure readable, we did not add the input and output signals of the MAL blocks shown in \cref{flt:sm_hw}.

\begin{figure}
  \centering
  \includegraphics[width=.8\linewidth]{images/cpu.pdf}
  \caption{
    Overview of the hardware blocks in the Sancus core.
    Lightly shaded blocks are part of the original \omsp{} design while the darkly shaded ones are added specifically for Sancus.
    Remember that, while we only draw two $\SM$ blocks for clarity, this number ($N_{\SM}$) can be chosen when synthesizing the core.
    Notice how the $\SM$ control unit takes the program counter (PC) and the memory address bus (MAB) as input to produce the violation signal using the memory access logic (MAL) circuits.
  }
  \label{flt:hwoverview}
\end{figure}

\subsection{The Compiler}
\label{sec:implementation:compiler}

Although the hardware modifications  enable software developers to create protected modules, doing this correctly is tedious,
as the module can have only one entry point, and as modules may need to implement their own call-stack to avoid leaking
the content of stack allocated variables to unprotected code or to other modules.
Hence, we have implemented a compiler extension based on LLVM~\cite{llvm} that deals with these low-level  details.
We have also implemented a support library that offers an API to perform some commonly used functions like calculating a \ac{MAC} of data.

Our compiler compiles standard C files.%
\footnote{We use Clang~\cite{clang} as our compiler frontend. This means any C-dialect accepted by Clang is supported.}
To benefit from Sancus, a developer only needs to indicate which functions should be part of the protected module being created,
which functions should be entry points and what data should be inside the protected section.
For this purpose, we offer three attributes -- \code{SM\_FUNC}, \code{SM\_ENTRY} and \code{SM\_DATA} -- that can be used to annotate functions and global variables.
%These annotations expect the name of the module as argument in order for the compiler to be able to differentiate
%multiple modules defined in a single file.

\subsubsection{Entry Points}
Since the hardware supports a single entry point per module only, the compiler implements multiple logical entry
points on top of the single physical entry point by means of a jump table.
The compiler assigns every logical  entry point a unique ID.
When calling a logical entry point, its ID is placed in a register before jumping to the physical entry point of the module.
The code at the physical entry point then jumps to the correct function based on the ID passed in the register.

When a module calls an external function, the same entry point is also used when this function returns.
This is implemented by using a special ID for the ``return entry point''.
If this ID is provided when entering the module, the address to return to is loaded from the module's stack.
Of course, this is only safe if stack switching is enabled.

\subsubsection{Stack Switching}

As discussed in \cref{sec:design:isolation}, it is preferable to place the runtime stack of software modules inside the data section.
Our compiler automatically handles everything needed to support multiple stacks.
For every module, space is reserved at a fixed location in its protected section for the stack.
The first time a module is entered, the stack pointer is loaded with the address of this start location of the stack.
When the module is exited, the current value of the stack pointer is stored in the protected section so that it can be restored when the module is reentered.

\subsubsection{Exiting Modules}

Our compiler ensures that no data is leaked through registers when exiting from a module.
When a module exits, either by calling an external function or by returning, any register that is not part of the calling convention is cleared.
That is, only registers that hold a parameter or a return value retain their value.

\subsubsection{Secure Linking}

Calls to protected modules are automatically instrumented to verify the called module.
This includes automatically calculating any necessary module keys and identities (\cref{sec:design:localcomm}).
Of course, a software provider needs to provide its key to the compiler for this function to work.

\subsection{Deployment}
\label{sec:implementation:deployment}

Since the identity of a module is dependent on its load addresses on node $N$, $\SP$ must be aware of these addresses in order to be able to calculate $K_{N,\SP,\SM}$.
Moreover, any identity hashes needed for secure linking will also be dependent on the load addresses of other modules.
Enforcing static load addresses is obviously not a scalable solution given that we target systems supporting dynamic loading of software modules by third-party software providers.

Given these difficulties, we felt the need to develop a proof-of-concept software stack providing a deployment solution.
Our stack consists of two parts: a set of tools used by $\SP$ to deploy $\SM$ on $N$ and host software running on $N$.
Note that this host software is \emph{not} part of any protected module and, hence, does not increase the size of the \ac{TCB}.

We will now describe the deployment process implemented by our software stack.
First, $\SP$ creates a relocatable \ac{ELF} file of $\SM$ and sends it to $N$.
The host software on $N$ receives this file, finds a free memory area to load $\SM$ and relocates it using a custom made dynamic \ac{ELF} loader.
Then, hardware protection is enabled for $\SM$ and a symbol table is sent back to $\SP$.
This symbol table contains the addresses of any global functions\footnote{For example, \code{libc} functions and I/O routines.} as well as the load addresses of all protected modules on $N$.
Using this symbol table, $\SP$ is able to reconstruct the exact same image of $\SM$ as the one loaded on $N$ which can then be used to calculate $K_{N,\SP,\SM}$.

Note that an alternative linking strategy is for $\SP$ to first request the node's symbol table, link the module locally and then send it to the node to be loaded.
This would simplify the node since the custom \ac{ELF} loader is not needed in this scheme.
However, since our toolchain does not support position-independent code, this would mean that the memory locations where the module is going to be loaded need to be reserved while $\SP$ links the module.
We feel that this two-phase deployment scheme adds more overall complexity than a simple \ac{ELF} loader.

Since the dynamic loader needs to inspect and update parts of the text section of modules, this process does not work when confidential loading is used.
Although our tools currently do not support the fully automatic loading of encrypted modules, it can be implemented as follows.
First, $\SP$ sends a request to $N$ indicating the sizes of the sections of the module it wants to load.
Then, the host software allocates memory for those sections and replies with a handle identifying the allocated memory and a symbol table.
Using this symbol table, $\SP$ links $\SM$ locally and sends the resulting image, together with the memory handle, back to $N$.
The host software on $N$ then loads it in the pre-allocated memory sections and enables its protection.

After $\SM$ has been deployed, the host software on $N$ provides an interface to be able to call its entry points.
This can be used by $\SP$ to attest that $\SM$ has not been compromised during deployment and that the hardware protection has been activated.

This interface is used to upload the identity hashes to $\SM$ of the modules it securely links to.
To this end, $\SP$ either calculates these hashes after it received the symbol table or, if it concerns modules belonging to a different software provider that use confidential loading, receives them from their respective providers.
Then, $\SP$ encrypts those hashes using $K_{N,\SP,\SM}$ and sends them to $\SM$ using the interface described above.
