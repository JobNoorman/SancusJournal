\documentclass[acmsmall]{acmart}

\acmJournal{TOPS}
\acmVolume{0}
\acmNumber{0}
\acmArticle{0}
\acmYear{0000}
\acmMonth{0}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{url}
\usepackage{adjustbox}
\usepackage{microtype}
\usepackage{textcomp}
\usepackage{graphicx}
\usepackage[english=american]{csquotes}

\usepackage[capitalize]{cleveref}
\crefname{figure}{Figure}{Figures}
\Crefname{figure}{Figure}{Figures}

\usepackage{enumitem}
\newlist{paraenum}{enumerate*}{1}
\setlist[paraenum]{label=(\arabic*)}

\usepackage{acronym}
\acrodef{TCB}{Trusted Computing Base}
\acrodef{MAC}{Message Authentication Code}
\acrodef{SPM}{Self Protecting Module}
\acrodef{ROP}{Return-Oriented Programming}
\acrodef{HVAC}{Heating, Ventilation, and Air Conditioning}
\acrodef{ELF}{Executable and Linkable Format}
\acrodef{IV}{Initialization Vector}
\acrodef{AE}{Authenticated Encryption}
\acrodef{AD}{Associated Data}
\acrodef{LUT}{Lookup Table}
\acrodef{IoT}{Internet of Things}
\acrodef{OS}{Operating System}
\acrodef{WAN}{Wide Area Network}
\acrodef{HAN}{Home Area Network}
\acrodef{WSN}{Wireless Sensor Network}

\usepackage[textsize=footnotesize]{todonotes}
\presetkeys{todonotes}{fancyline}{}
\setlength{\marginparwidth}{2.4cm}
\setlength{\marginparsep}{3pt}
\input{todonotes-hack.tex}

\newcommand{\ie}{i.e.,}
\newcommand{\eg}{e.g.,}

\usepackage{tikz}
\usepackage{pgfplots}
\pgfplotsset{compat=1.10}
\usetikzlibrary{fit, backgrounds, shapes, circuits.logic.US, circuits.logic.IEC, circuits.ee.IEC, decorations.pathreplacing, arrows, matrix, intersections, positioning, decorations.markings}

% some tikz styles used in figures
\tikzstyle{processor}=[draw, fill=gray!20!white]
\tikzstyle{sm}=[draw]
\tikzstyle{sp}=[draw, circle]
\tikzstyle{ip}=[draw, diamond]
\tikzset{>=stealth'}

\tikzset{circuit declare symbol=io,
         set io graphic={inner sep=2}}
\tikzset{circuit declare symbol=reg,
         set reg graphic={draw, inner sep=5}}
\tikzset{circuit declare symbol=cmp,
         set cmp graphic={draw, inner sep=5}}

\newcommand{\omsp}{\mbox{openMSP430}} % mbox to prevent hyphenation
\newcommand{\spongent}{\textsc{spongent}}
\newcommand{\spongentvar}{\spongent-128/128/8}
\newcommand{\spongewrap}{\textsc{SpongeWrap}}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\instr}[1]{\texttt{#1}}
\newcommand{\instrarg}[1]{\textit{#1}}

\DeclareMathOperator{\IP}{\textit{IP}}
\DeclareMathOperator{\SP}{\textit{SP}}
\DeclareMathOperator{\SM}{\textit{SM}}
\DeclareMathOperator{\No}{\textit{No}}
\DeclareMathOperator{\kdf}{\textit{kdf}}
\DeclareMathOperator{\aee}{\textit{aead-encrypt}}
\DeclareMathOperator{\aed}{\textit{aead-decrypt}}
\DeclareMathOperator{\mac}{\textit{mac}}
\DeclareMathOperator{\hash}{\textit{hash}}

\begin{document}

\newcommand{\thetitle}{Sancus 2.0: A Low-Cost Security Architecture for IoT Devices}
%\markboth{Job Noorman et al.}{\thetitle}
\title{\thetitle}

\author{Job Noorman}
\author{Jo Van Bulck}
\author{Jan Tobias Mühlberg}
\author{Frank Piessens}
\affiliation{iMinds-DistriNet, KU Leuven}
\author{Pieter Maene}
\author{Bart Preneel}
\author{Ingrid Verbauwhede}
\affiliation{iMinds-COSIC, KU Leuven}
\author{Johannes Götzfried}
\author{Tilo Müller}
\author{Felix Freiling}
\affiliation{FAU Erlangen-Nürnberg}

\begin{abstract}
The Sancus security architecture for networked embedded devices was
proposed in 2013 at the USENIX Security conference. It supports remote
(even third-party) software installation on devices while maintaining
strong security guarantees. More specifically, Sancus can remotely
attest to a software provider that a specific software module is running
uncompromised, and can provide a secure communication channel between
software modules and software providers. Software modules can securely
maintain local state, and can securely interact with other software
modules that they choose to trust.

Over the past three years, significant experience has been gained with
applications of Sancus, and several extensions of the architecture have
been investigated -- both by the original designers as well as by
independent researchers. Informed by these additional research results,
this journal version of the Sancus paper describes an improved design
and implementation, supporting additional security guarantees (such as
confidential deployment) and a more efficient cryptographic core.

We describe the design of Sancus 2.0 (without relying on any prior
knowledge of Sancus), and develop and evaluate a prototype FPGA
implementation. The prototype extends an MSP430 processor with hardware
support for the memory access control and cryptographic functionality
required to run Sancus. We report on our experience with using Sancus in
a variety of application scenarios, and discuss some important avenues
of ongoing and future work.
\end{abstract}

\thanks{%
This work has been supported in part by the Intel Lab's University Research Office.
This research is also partially funded by the Research Fund KU Leuven, and by the Research Foundation - Flanders (FWO).
This work was partially supported by the German Research Foundation (DFG) as part of the Transregional Collaborative Research Centre ``Invasive Computing'' (SFB/TR 89).
Job Noorman, Jo Van Bulck, and Pieter Maene are supported by a doctoral grant of the Research Foundation - Flanders (FWO).

Author's addresses: \{Job.Noorman, Jo.VanBulck, JanTobias.Muehlberg, Frank.Piessens\}@cs.kuleuven.be, \{Pieter.Maene, Bart.Preneel, Ingrid.Verbauwhede\}@esat.kuleuven.be, \{Johannes.Goetzfried, Tilo.Mueller, Felix.Freiling\}@cs.fau.de%
}

\begin{CCSXML}
  <ccs2012>
    <concept>
      <concept_id>10002978.10003001.10003003</concept_id>
      <concept_desc>Security and privacy~Embedded systems security</concept_desc>
      <concept_significance>500</concept_significance>
    </concept>
    <concept>
      <concept_id>10010520.10010553.10010562</concept_id>
      <concept_desc>Computer systems organization~Embedded systems</concept_desc>
      <concept_significance>300</concept_significance>
    </concept>
      <concept>
      <concept_id>10010520.10010553.10003238</concept_id>
      <concept_desc>Computer systems organization~Sensor networks</concept_desc>
      <concept_significance>100</concept_significance>
    </concept>
    <concept>
      <concept_id>10010520.10010553.10010559</concept_id>
      <concept_desc>Computer systems organization~Sensors and actuators</concept_desc>
      <concept_significance>100</concept_significance>
    </concept>
  </ccs2012>
\end{CCSXML}

\ccsdesc[500]{Security and privacy~Embedded systems security}
\ccsdesc[300]{Computer systems organization~Embedded systems}
\ccsdesc[100]{Computer systems organization~Sensor networks}
\ccsdesc[100]{Computer systems organization~Sensors and actuators}

\keywords{Protected Module Architectures, Embedded systems security, Trusted computing, Software security engineering}

\maketitle

\renewcommand{\shortauthors}{Job Noorman et al.}

\input{introduction.tex}
\input{problem.tex}
\input{design.tex}
\input{implementation.tex}
\input{applications.tex}
\input{evaluation.tex}
\input{related-work.tex}
\input{conclusion.tex}

\section{Availability}
To ensure reproducibility and verifiability of our results, we make the hardware design and the software of our
prototype publicly available.
All source files, binary packages and documentation can be found at \url{https://distrinet.cs.kuleuven.be/software/sancus/}.

\bibliographystyle{ACM-Reference-Format}
\bibliography{paper}

\end{document}
