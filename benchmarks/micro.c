#include "common.h"

#include <stdio.h>
#include <stdlib.h>

#include <msp430.h>

#include <sancus/sm_support.h>
#include <sancus_support/uart.h>
#include <sancus_support/tsc.h>

DECLARE_TSC_TIMER(timer);

static void time_wrap_unwrap(size_t size)
{
    printf("Timing wrap/unwrap of %u bytes\n", size);

    uint8_t key[SANCUS_KEY_SIZE] = {0xde, 0xad, 0xbe, 0xef,
                                    0xca, 0xfe, 0xba, 0xbe,
                                    0xde, 0xad, 0xbe, 0xef,
                                    0xca, 0xfe, 0xba, 0xbe};
    uint8_t tag[SANCUS_TAG_SIZE];
    uint8_t ad[] = {0x12, 0x34};
    uint8_t* body = malloc(size);
    uint8_t* cipher = malloc(size);

    TSC_TIMER_START(timer);
    sancus_wrap_with_key(key, ad, sizeof(ad), body, size, cipher, tag);
    TSC_TIMER_END(timer);

    TSC_TIMER_START(timer);
    int result = sancus_unwrap_with_key(key, ad, sizeof(ad),
                                        cipher, size, tag, body);
    TSC_TIMER_END(timer);

    if (!result)
        puts("Unwrapping failed");

    free(body);
    free(cipher);
}

static void time_mac(size_t size)
{
    printf("Timing MAC of %u bytes\n", size);

    uint8_t key[SANCUS_KEY_SIZE] = {0xde, 0xad, 0xbe, 0xef,
                                    0xca, 0xfe, 0xba, 0xbe,
                                    0xde, 0xad, 0xbe, 0xef,
                                    0xca, 0xfe, 0xba, 0xbe};
    uint8_t tag[SANCUS_TAG_SIZE];
    uint8_t* body = malloc(size);

    TSC_TIMER_START(timer);
    sancus_tag_with_key(key, body, size, tag);
    TSC_TIMER_END(timer);

    free(body);
}

static void time_enable(size_t size)
{
    printf("Timing enable of %u bytes\n", size);

    uint8_t* ts = malloc(size);
    struct SancusModule sm = {0, 0, NULL, ts, ts + size, NULL, NULL};

    TSC_TIMER_START(timer);
    sm_id id = sancus_enable(&sm);
    TSC_TIMER_END(timer);

    if (id == 0)
        puts("Enabling failed");
    else
        printf("Enabled with id %u\n", id);
}

static void time_enable_encrypted(size_t size)
{
    printf("Timing encrypted enable of %u bytes\n", size);

    uint8_t* ts = malloc(size);
    uint8_t ad[] = {0xff, 0xff};
    uint8_t tag[SANCUS_TAG_SIZE];

    // Vendor key for SP 0x0000
    uint8_t key[SANCUS_KEY_SIZE] = {0xcb, 0x37, 0xad, 0xdb,
                                    0xad, 0x90, 0x21, 0x0f,
                                    0x79, 0x5b, 0xf2, 0xd3,
                                    0x04, 0x46, 0x92, 0x0d};

    sancus_wrap_with_key(key, ad, sizeof(ad), ts, size, ts, tag);
    struct SancusModule sm = {0, 0, NULL, ts, ts + size, NULL, NULL};

    TSC_TIMER_START(timer);
    sm_id id = sancus_enable_wrapped(&sm, 0xffff, tag);
    TSC_TIMER_END(timer);

    if (id == 0)
        puts("Enabling failed");
    else
        printf("Enabled with id %u\n", id);
}

int main()
{
    init_hardware();

    puts("main started");

    time_wrap_unwrap(256);
    time_wrap_unwrap(512);
    time_wrap_unwrap(1024);

    time_mac(256);
    time_mac(512);
    time_mac(1024);

    time_enable(256);
    time_enable(512);
    time_enable(1024);

    time_enable_encrypted(256);
    time_enable_encrypted(512);
    time_enable_encrypted(1024);
}
