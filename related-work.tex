\section{Related Work}
\label{sec:related}

Ensuring strong isolation of code and data is a challenging problem. Many
solutions have been proposed, ranging from hardware-only to software-only
mechanisms, both for high-end and low-end devices. Below we discuss
research that is more directly related to Sancus. For an extended
comparison of hardware-based trusted computing architectures we refer the
reader to \cite{maene:hardware-based-iso-attest}.

\subsection{Isolation in High-End Devices}
The Multics~\cite{corbato1965introduction} operating system marked the start of the use of protection rings to isolate less trusted software. Despite decades of research, high-end devices equipped with this feature are still being attacked successfully. More recently, research has switched to focus on the isolation of software modules with a minimal TCB by relying on recently added hardware support. McCune et al.~propose Flicker~\cite{mccune2008Flicker}, a system that relies on a TPM chip and trusted computing functionality of modern CPUs, to provide strong isolation of modules with a TCB of only 250~LOCs. Subsequent research~\cite{mccune2010TrustVisor,azab2011sice,sahita2009criticalAppsOnMobile,fides} focuses on various techniques to reduce the number of TPM accesses and significantly increase performance, for example by taking advantage of hardware support for virtual machines.

ARM TrustZone~\cite{alves:trustzone} implements hardware based access
control to use a physical core as two virtual processors so as to execute
security critical applications in their own \enquote{world}, in isolation
from the normal world.  The secure world runs its own \ac{OS}, libraries
and applications, which mutually trust each other.  TrustZone for the v8-M
architecture~\cite{arm:trustzone-v8-m} employs a \enquote{secure gateway}
instruction to enter the secure world at specific entry point addresses,
providing similar but more coarsely-grained isolation properties than
Sancus.
 
More recently, Intel started shipping x86 processors equipped with Software
Guard Extensions (SGX)~\cite{sgx} that allows the execution of
security-critical code via hardware-enforced individually isolated
\emph{enclaves} in a shared address space, managed by an untrusted \ac{OS}.
SGX also provides functionality for local and remote attestation and for
data sealing~\cite{sgx-attestation}.

While the aforementioned architectures focus on isolating relatively small,
security-sensitive application components, an alternative line of work seeks to
protect largely unmodified legacy applications from an untrusted \ac{OS}.
Overshadow~\cite{chen2008overshadow} and InkTag~\cite{hofmann2013inktag} employ
a trusted hypervisor, whereas Haven~\cite{baumann2014haven} leverages Intel SGX
processor extensions to isolate application binaries.  The untrusted \ac{OS}
continues to provide resource management and application services.  However,
due to the complex nature of legacy operating system interfaces and hardware,
these designs are exposed to a new class of powerful side-channel
attacks~\cite{xu2015controlled}.  Compared to higher-end MMU-based systems,
Sancus can be considered less susceptible to such threats considering the
elementary design of its security extensions, as well as the underlying
processor.

The idea of deriving module specific keys from a master key using (a digest of) the module's code is also used by the On-board Credentials project~\cite{obc}.
They use existing hardware features to enforce the isolated execution of \emph{credential programs} and securely store secret keys.
Only one credential program can effectively be loaded at any single moment, but the concept of \emph{families} is introduced to be able to share secrets between different programs.
Although secure communication is implemented using symmetric cryptography, they rely on public key cryptography during the deployment process.

\subsection{Isolation in Low-End Devices}
While recent research results on commodity computing platforms are promising, the hardware components they rely on require energy levels that significantly exceed what is available to many embedded devices such as pacemakers~\cite{halperin2008pacemakers} and sensor nodes. A lack of strong security measures for such devices significantly limits how they can be applied and vendors may be required to develop closed systems or leave their system vulnerable to attack.

Sensor operating systems and applications, for example, were initially compiled into a monolithic and static image without safety or security considerations, as in early versions of TinyOS~\cite{Levis:2012:EDT:2387880.2387901}. The reality that sensor deployments are long-lived, and that the full set of modules and their detailed functionality is often unknown at development time, resulted in dynamic modular operating systems such as SOS~\cite{Han:2005:DOS:1067170.1067188} or Contiki~\cite{Dunkels:2006:RDL:1182807.1182810}. As stated in the introduction of this paper, the availability of networked modular update capability creates new threats, particularly if the software modules originate from different stakeholders and can no longer be fully trusted. Many ideas have been put forward to address the safety concerns of these shared environments, and solutions to provide memory protection, isolation, and (fair) multithreading have appeared. t-kernel~\cite{gu_t-kernel:_2006} rewrites code on the sensor at load time. Coarse-grained memory protection (basically MMU emulation) is available for the SOS operating system by sandboxing in the Harbor system~\cite {Kumar:2007:HSM:1236360.1236404} through a combination of backend compile time rewriting and run time checking on the sensor. Safe TinyOS~\cite{Cooprider:2007:EMS:1322263.1322283} equally uses a combination of backend compile time analysis and minimal run time error handlers to provide type and memory safety. Java's language features and the Isolate mechanism are used on the Sun SPOT embedded platform using the Squawk VM~\cite{conf/vee/SimonCCDW06}. SenShare~\cite{Leontiadis:2012:STS:2187181.2187188} provides a virtual machine for TinyOS applications. %It is important to note that all these works refer to traditional hardware mechanisms as too complex for the embedded platform yet all provided solutions have software-induced overhead and increase the software TCB.
While these proposed solutions do not require any hardware modifications, they all incur a software-induced overhead. Moreover, third-party software providers must rely on the infrastructure provider to correctly rewrite modules running on the same device.

To increase security of embedded devices, Strackx~et~al.~\cite{KULeuven-277417} introduced the idea of a program counter-based access control model, but without providing any implementation. Agten et~al.~\cite{agten2012fullabstraction} prove that isolation of code and data within such a model only relies on the vendor of the module and cannot be influenced by other modules on the same system. El~Defrawy et~al.~\cite{smart} implemented hardware support for allowing attestation that a module executed correctly without any interference, based on a similar access control model. While this is a significant step forward, it does not provide isolation, as sensitive data cannot be kept secret from other modules between invocations.
TrustLite~\cite{trustlite}, on the other hand, features an Execution-Aware Memory Protection Unit (EA-MPU) that records program counter-based memory access rules in a configurable hardware table.
Compared to Sancus, this allows for more complex policies, such as multiple private data sections per module, or protected data sharing between two or more modules.
TrustLite, however, relies on a trusted Secure Loader software entity to initialize the EA-MPU table at boot time, and does not allow modules to be unloaded at run time.
More recently, the TyTAN~\cite{tytan} architecture extends TrustLite with dynamic loading, and local and remote attestation guarantees for isolated tasks from mutually distrusting stakeholders.
Their approach to attestation resembles Sancus' in that they derive keys from task identities and a hardware-level platform key.
In contrast to Sancus however, TyTAN relies on a trusted software runtime to measure task identities, and to guard inter-module authenticated communication.

Java~Card~\cite{oracle:javacard305,globalplatform} by Oracle is a smart
card technology with applications in, e.g., mobile communications and
electronic citizen IDs. Java Card provides an isolated environment, the
Java Card~VM and the Applet Firewall, to execute Java Card Applets in
isolation from the operating system and other such applets. The technology
features a range of symmetric and public key cryptographic algorithms but
does not provide lightweight embedded ciphers such as Sancus'
\spongewrap~\cite{spongewrap}. Extensions for remote attestation are under
development. Overall, Java Card can be used to implement many features of
Sancus. Yet, Java Card has a larger hardware and software
\ac{TCB} and exhibits different performance characteristics than
our approach, impeding deployment in some of the scenarios outlined in
\cref{sec:apps}.

